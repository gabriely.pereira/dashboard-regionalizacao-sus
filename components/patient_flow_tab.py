from dash import Input, Output, html
from dash import dcc
import dash_bootstrap_components as dbc

from helpers import get_items_level


dropdown_items_level = dcc.Dropdown(
   options=get_items_level('RJ', 2019, 'mun'),
   searchable=True,
   clearable=False,
   id="level_items_drop",
)

map_option = html.Div(
    [
        dbc.RadioItems(
            id="map_radios",
            inline=True,
            labelCheckedClassName="active",
            options=[
                {"label": "Fluxos", "value": "flow"},
                {"label": "Internações", "value": "scatter"},
            ],
            value="flow",
        ),
    ],
    className="radio-group",
)

slider_year = dcc.Slider(1995, 2019, 1,
                value=2019,
                id='year_slider_flow',
                marks={
                    str(year): {
                        "label": str(year),
                        "style": {"color": "#7fafdf"},
                    }
                    for year in range(1995, 2020, 1)
                },
)

def tab_flow(app): 
    return html.Div(
            className="text-white bg-dark",
            children=[
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(dbc.Card(dbc.CardBody([
                            dbc.Row(
                                [
                                    dbc.Col(html.H5(["Movimentação de pacientes em ",
                                                    html.Span(id="year_span_flow"),
                                                    html.Span(id="diagnosis_span_flow")], className="card-title"), width=9),
                                    dbc.Spinner(dbc.Col(map_option, width=3), delay_hide=1000),
                                ]
                            ), 
                            html.Div(id="map_output_flow"), 
                            html.Div(html.Img(src=app.get_asset_url("legenda_fluxo.png"), height="60", width="230"), id="legend"),
                        ], className="wrapper"),color="dark", inverse=True), width=8),
                        dbc.Col([
                            html.Div(id="choose_bar_level"), 
                            html.Div(dropdown_items_level, id="dropdown_bar_level"),
                            html.Div(className="half-br"),
                            dbc.Card(dbc.Spinner(dbc.CardBody(id="top5_inflow_output"), delay_hide=1000), color="dark", inverse=True),
                            html.Div(className="half-br"),
                            dbc.Card(dbc.Spinner(dbc.CardBody(id="top5_outflow_output"), delay_hide=1000), color="dark", inverse=True),
                        ], width=4),
                    ]
                ),
                html.Div(className="half-br"),
                dbc.Row(
                    [
                        dbc.Col(dbc.Card(dbc.CardBody([
                            slider_year,
                            html.P(id="available_data_flow", className='text-muted text-end small font-weight-light'),
                        ]), color="dark", inverse=True), width=8),
                        dbc.Col(dbc.Card(dbc.CardBody(html.P([html.Span("Fontes: ", className="font-weight-bold"),
                                                    html.Span("Internações hospitalares do SIHSUS (Sistema de Informações Hospitalares do SUS). Malha territorial do IBGE. Regiões e Macrorregiões de Saúde do SAGE (Sala de apoio à Gestão Estratégica).")
                                                    ], className="no-margin font-size-12px")
                                        ), color="dark", inverse=True), width=4),
                    ]
                ),
            ]
        )