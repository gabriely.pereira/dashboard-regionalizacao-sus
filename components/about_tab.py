from dash import html
from dash import dcc
import dash_bootstrap_components as dbc

def tab_about(app):
    return html.Div([html.Br(),
            dbc.Row([dbc.Col(dbc.Card(dbc.CardBody([
                    dcc.Markdown('''
                        ## Sobre
                        ------

                        Esta plataforma foi resultado da pesquisa de mestrado de Gabriely R. Pereira, do Intituto de Matemática e Estatística da Universidade de São Paulo (IME-USP), orientada pelo Prof. Fabio Kon. 
                        Ela contou com o auxílio financeiro do CNPq e da FAPESP, vinculado ao InterSCity, o INCT da Internet do Futuro para Cidades Inteligentes (https://interscity.org/). 
                        Este projeto também contou com a colaboração de parceiros, em especial o Instituto de Estudos para Políticas de Saúde (IEPS; https://ieps.org.br/).  

                    '''),
                    html.Br(),
                    dbc.Row([dbc.Col(html.Img(src=app.get_asset_url('ime-logo.png'), height="40"), width=4),
                             dbc.Col(html.Img(src=app.get_asset_url('CNPq-logo.png'), height="50"), width=3),
                             dbc.Col(html.Img(src=app.get_asset_url('fapesp-logo.png'), height="60"), width=3),
                            ], className='justify-content-md-center'),
                    html.Br(),
                    dbc.Row([
                             dbc.Col(html.Img(src=app.get_asset_url('interscity_logo.png'), height="60"), width=4),
                             dbc.Col(html.Img(src=app.get_asset_url('ieps_logo.png'), height="50"), width=3)
                            ], className='justify-content-md-center'),
                    html.Br(),
                ], className="container"), color="dark", inverse=True), width=8)
            ], className='justify-content-md-center')])
