from dash import html
from dash import dcc
import dash_bootstrap_components as dbc

def tab_methodology(app):
    return html.Div([html.Br(),
            dbc.Row([dbc.Col(dbc.Card(dbc.CardBody([
                    dcc.Markdown('''
                        ## Metodologia
                        ------
                    '''),
                    html.Br(),
                    dbc.Row([
                        dbc.Col(html.Img(src=app.get_asset_url('br-network.png'), height="220"), width=3),
                        dbc.Col(dcc.Markdown('''
                            O processo de regionalização de saúde tem o objetivo de garantir o direito à saúde da população, reduzindo as desigualdades sociais e territoriais. 
                            Uma região de saúde é composta pela coleção de municípios adjacentes, organizados para promover uma maior eficiência no acesso aos serviços de saúde. 
                            Esta plataforma tem como objetivo promover análises do processo de regionalização da saúde utilizando dados de fluxos de pacientes entre municípios brasileiros de 1995 a 2019, a partir do Sistema de Informações Hospitalares do SUS (SIHSUS). 
                            Através de técnicas de **sistemas complexos**, analisamos a rede de movimentação de pacientes propondo meios de avaliação e visualização do comportamento das regiões de saúde em cada estado brasileiro. 
                            Para isso, recorremos a métodos de detecção de comunidades em redes complexas, modelagem de fluxos de oferta e demanda, e ferramentas de visualização de dados georreferenciados. 
                            Com eles, analisamos a relação entre a divisão das regiões e o fluxo dos pacientes, e definimos um índice para as regiões baseado nas taxas de permanência e atração de pacientes.  
                        '''), width=9)]),
                    html.Br(),
                    dcc.Markdown('''
                        Para produzir as análises desta plataforma, um sistema complexo foi modelado para representar o fluxo intermunicipal de internações hospitalares atendidas pelo SUS nas últimas décadas. 
                        Em nosso caso, os *elementos do sistema* são municípios de um mesmo estado brasileiro e as *ligações entre elementos* indicam a movimentação dos pacientes do município de residência ao município de atendimento. 
                        Em alguns momentos as análises mudam de nível, de municipal para regional. Nesses momentos os *elementos* são regiões ou macrorregiões de saúde. 
                        De toda forma, todas as análises buscam investigar o comportamento ou eficiência da regionalização da saúde nos estados estudados.  
                    '''),
                    html.Br(),
                    dbc.Row([
                        dbc.Col(html.Img(src=app.get_asset_url('comunidades.png'), height="220"), width=2),
                        dbc.Col([html.Br(), dcc.Markdown('''
                            A partir da rede de fluxo intermunicipal de pacientes, podemos aplicar a técnica de **detecção de comunidades** 
                            para encontrar possíveis agrupamentos de municípios a partir dos dados. Como as 
                            regiões de saúde são, por definição, agrupamentos de municípios adjacentes pertencentes a um mesmo estado, podemos fazer uma análise de *comparação dos agrupamentos 
                            sugeridos pela solução orientada pelos dados e os agrupamentos definidos na organização original das regiões de saúde*.  
                            Outra abordagem utilizada no projeto é o cálculo de um índice que indique o nível de permanência e atração de pacientes de uma região 
                            de saúde a partir do fluxo de pacientes. 
                        ''')], width=10)]),
                    html.Br(),
                    dcc.Markdown('''
                        **Esperamos que a iniciativa em desenvolver uma plataforma web, disponibilizando essas análises em um *dashboard*, 
                        possa incentivar pesquisas sobre o processo de regionalização da saúde no SUS, 
                        além de promover tomadas de decisão por parte dos gestores de saúde pública.**  
                    '''),
                    html.Br(),
                    dcc.Markdown('''  
                        ### A plataforma
                        ------

                        Os códigos de todos os testes e implementações dos métodos estão disponíveis em um repositório GitLab (https://gitlab.com/interscity/health/scripts-sus-regionalization), 
                        contendo também os scripts de processamento das bases que geram os resultados para cada estado da federação (na pasta *"prepare/scripts/"*).  
                    '''),
                    html.Br(),
                    dcc.Markdown('''
                        Para gerar novas visualizações de outros estados brasileiros, deve-se seguir as seguintes etapas:  
                          1. Ter os arquivos iniciais ('panel_mun_cir.csv', 'Regioes de Saude.dta', 'Macrorregioes de Saude.dta', e a base SIH pré-processada para o estado);  
                          2. Rodar os 5 *scripts* para todas as opções possíveis em cada caso (municipal, regional ou macrorregional; cada *script* explica como rodá-lo);  
                          3. Adicionar os arquivos gerados pelos scripts nas pastas correspondentes no repositório do *dashboard* (https://gitlab.com/interscity/health/dashboard-sus-regionalization).  
                    '''),
                    html.Br(),
                    dcc.Markdown('''
                        Para obter mais detalhes da metodologia, [aqui](https://gitlab.com/gabriely.pereira/qualificacao/-/raw/master/TextoGabriely.pdf) está o texto da dissertação que deu origem ao projeto.  
                        Para mais detalhes do processo de regionalização da saúde no Brasil, [aqui](https://ieps.org.br/wp-content/uploads/2022/06/IEPS_Estudo_Institucional_07.pdf) está o Estudo Institucional do IEPS sobre a regionalização.  
                    '''),
                ], className="container"), color="dark", inverse=True), width=9),
            ], className='justify-content-md-center'), html.Br()])
