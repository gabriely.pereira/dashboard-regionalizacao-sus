from dash import Input, Output, html
from dash import dcc
import dash_bootstrap_components as dbc

slider_year = dcc.Slider(1995, 2019, 1,
                value=2019,
                id='year_slider_coeff',
                marks={
                    str(year): {
                        "label": str(year),
                        "style": {"color": "#7fafdf"},
                    }
                    for year in range(1995, 2020, 1)
                },
) 

slider_atracao = dcc.Slider(0, 1, 0.1,
                    value=0.2,
                    id='atracao_slider_coeff',
                    marks={
                        str(b/10): {
                            "label": str(b/10),
                            "style": {"color": "rgb(223, 175, 127)"},
                        }
                        for b in range(0, 10, 1)
                    },
)

def tab_coefficients(app):
    return html.Div(
            className="text-white bg-dark",
            children=[
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(dbc.Card(dbc.CardBody([
                            dbc.Row(
                                [
                                    dbc.Col([html.H5(["Combinando ", 
                                                    html.Span(dbc.Badge("permanência", pill=True, className="ms-1", id="permanencia-target")), " e ",
                                                    html.Span(dbc.Badge("atração", pill=True, className="ms-1", id="atracao-target")), " de pacientes em ", 
                                                    html.Span(id="year_span_coeff"),
                                                    html.Span(id="diagnosis_span_coeff")], className="card-title"), 
                                            dbc.Popover("Dentre os residentes da região, o nível de permanência é a taxa dos que permanecem na região para receber o atendimento hospitalar.", 
                                                                target="permanencia-target", body=True, trigger="hover"),
                                            dbc.Popover("Dentre os atendimentos na região, o nível de atração é a taxa dos pacientes não residentes da região.", 
                                                                target="atracao-target", body=True, trigger="hover")
                                            ], width=9),
                                ]
                            ), 
                            dbc.Spinner(html.Div(id="map_output_coeff"), delay_hide=1000), 
                            html.Div(html.Img(src=app.get_asset_url("legenda_coeff.png"), height="200", width="105"), id="legend_coeff"),
                        ], className="wrapper"),color="dark", inverse=True), width=7),
                        dbc.Col([
                            dbc.Row([
                                dbc.Col(dbc.Card(dbc.CardBody([html.H6("Taxa de permanência", className="card-title"), dbc.Spinner(html.Div(id="mini_lofi"), delay_hide=1000)]), color="dark", inverse=True), width=6),
                                dbc.Col(dbc.Card(dbc.CardBody([html.H6("Taxa de atração", className="card-title"), dbc.Spinner(html.Div(id="mini_1-lifo"), delay_hide=1000)]), color="dark", inverse=True), width=6)]),
                            html.Div(className="half-br"),
                            dbc.Row([
                                dbc.Col(dbc.Card(dbc.CardBody([html.H6("Cálculo do taxa de permanência", className="card-title"), 
                                                               html.Center(html.Img(src=app.get_asset_url("regiao-lofi-bw-info.png"), height="180"))]), color="dark", inverse=True), width=6),
                                dbc.Col(dbc.Card(dbc.CardBody([html.H6("Cálculo do taxa de atração", className="card-title"), 
                                                               html.Center(html.Img(src=app.get_asset_url("regiao-lifo-bw-info.png"), height="180"))]), color="dark", inverse=True), width=6)]),
                            html.Div(className="half-br"),
                            #dbc.Card(dbc.CardBody(), color="dark", inverse=True, className="invisible"),
                            dbc.Card(dbc.CardBody([dbc.Row([dbc.Col(html.H6("A combinação é feita pela média harmônica ponderada entre:", className="card-title"), width=11), 
                                                            dbc.Col(dbc.Badge("i", pill=True, className="ms-1", id="info-target"), width=1)]),
                                                   dbc.Popover("Método inspirado nos coeficientes LIFO (Little In From Outside) e LOFI (Little Out From Inside) de Elzinga e Hogarty.", 
                                                                target="info-target", body=True, trigger="hover"),
                                                   html.Center([html.Code("Taxa de permanência e ß * Taxa de atração"), ", onde ß é:"]),
                                                   slider_atracao]), color="dark", inverse=True),
                        ], width=5),
                    ]
                ),
                html.Div(className="half-br"),
                dbc.Row(
                    [
                        dbc.Col(dbc.Card(dbc.CardBody([
                            slider_year,
                            html.P(id="available_data_coeff", className='text-muted text-end small font-weight-light'),
                        ]), color="dark", inverse=True), width=7),
                        dbc.Col(dbc.Card(dbc.CardBody(html.P([html.Span("Fontes: ", className="font-weight-bold"),
                                                    html.Span("Internações hospitalares do SIHSUS (Sistema de Informações Hospitalares do SUS). Malha territorial do IBGE. Regiões e Macrorregiões de Saúde do SAGE (Sala de apoio à Gestão Estratégica).")
                                                    ], className="no-margin font-size-12px")
                                        ), color="dark", inverse=True), width=5),
                    ]
                ),
            ]
        )
