from dash import Input, Output, html
from dash import dcc
import dash_bootstrap_components as dbc

slider_year = dcc.Slider(1995, 2019, 1,
                value=2019,
                id='year_slider_comm',
                marks={
                    str(year): {
                        "label": str(year),
                        "style": {"color": "#7fafdf"},
                    }
                    for year in range(1995, 2020, 1)
                },
)


def tab_communities(app):
    return html.Div(
            className="text-white bg-dark",
            children=[
                html.Br(),
                dbc.Row(
                    [
                        dbc.Col(dbc.Card(dbc.CardBody([
                            dbc.Row(
                                [
                                    dbc.Col(html.H5(["Comparação das divisões em ",
                                                    html.Span(id="year_span_comm"),
                                                    html.Span(id="diagnosis_span_comm")], className="card-title"), width=9), 
                                ]
                            ), 
                            dbc.Spinner(html.Div(id="map_output_comm"), delay_hide=1000), 
                            html.Div(html.Img(src=app.get_asset_url("legenda_comunidades.png"), height="130", width="230"), id="legend_comm"),
                        ], className="wrapper"),color="dark", inverse=True), width=7),
                        dbc.Col([
                            dbc.Row([
                                dbc.Col(dbc.Card(dbc.CardBody([html.H6("Divisão das regiões de saúde", className="card-title"), dbc.Spinner(html.Div(id="mini_shape_reg"), delay_hide=1000)]), color="dark", inverse=True), width=6),
                                dbc.Col(dbc.Card(dbc.CardBody([html.H6("Divisão sugerida pelo algoritmo", className="card-title"), dbc.Spinner(html.Div(id="mini_shape_comm"), delay_hide=1000)]), color="dark", inverse=True), width=6)]),
                            html.Div(className="half-br"),
                            dbc.Card(dbc.CardBody([html.H4([html.Span(id="similarity_level", className="color-lblue"), 
                                                            html.Span(" de semelhança", className="font-size-18px")]), 
                                                            html.P(["entre a divisão das regiões de saúde e a divisão sugerida pelo ", html.Span(dbc.Badge("Infomap", className="ms-1", id="infomap-target"))]),
                                                            dbc.Popover("Infomap é um algoritmo que detecta comunidades a partir de uma rede de informações. Neste caso, ele detectou regiões dentro do estado a partir da rede de movimentação de pacientes.", 
                                                                        target="infomap-target", body=True, trigger="hover")]), color="dark", inverse=True),
                            html.Div(className="half-br"),
                            dbc.Card(dbc.CardBody(id="line_chart_output_comm"), color="dark", inverse=True),
                        ], width=5),
                    ]
                ),
                html.Div(className="half-br"),
                dbc.Row(
                    [
                        dbc.Col(dbc.Card(dbc.CardBody([
                            slider_year,
                            html.P(id="available_data_comm", className='text-muted text-end small font-weight-light'),
                        ]), color="dark", inverse=True), width=7),
                        dbc.Col(dbc.Card(dbc.CardBody(html.P([html.Span("Fontes: ", className="font-weight-bold"),
                                                    html.Span("Internações hospitalares do SIHSUS (Sistema de Informações Hospitalares do SUS). Malha territorial do IBGE. Regiões e Macrorregiões de Saúde do SAGE (Sala de apoio à Gestão Estratégica).")
                                                    ], className="no-margin font-size-12px")
                                        ), color="dark", inverse=True), width=5),
                    ]
                ),
            ]
        )