import math
import statistics

import pandas as pd
import geopandas as gpd
import numpy as np
import pydeck as pdk
import dash_deck
from dash import html
from shapely.ops import cascaded_union

def get_diagnosis(diag='entire'):
    diag_dict = {
        'all': 'Todos os diagnósticos',
        'neoplasm': 'Neoplasias',
        'circulatory_system': 'Doenças do aparelho circulatório',
        'parasitic_diseases': 'Doenças parasitárias',
        'musculoskeletal_system': 'Doenças do sistema osteomuscular',
        'external_causes': 'Causas externas',

        'eye_ear_diseases': 'Doenças do olho e ouvido',
        'nervous_system': 'Doenças do sistema nervoso',
        'genitourinary_system': 'Doenças do aparelho geniturinário',

        'pregnancy': 'Gravidez',
        'diabetes': 'Diabetes',
        'mental_disorders': 'Transtornos mentais',
        'skin_diseases': 'Doenças da pele'
    }
    if(diag == 'entire'): return diag_dict
    else: return diag_dict[diag]

def get_items_level(uf, year, level_value):
    df = pd.read_csv("./data/%s/csv/%s_sih_flow.csv"%(uf, level_value))
    df['from'] = df['from'].map(lambda x: x if(eval(x)['name']) else None)
    df['to'] = df['to'].map(lambda x: x if(eval(x)['name']) else None)
    df = df[df['year']==year].dropna()
    df['from'] = df['from'].map(eval)
    df['from_name'] = df["from"].apply(lambda f: f["name"])

    return np.unique(df['from_name'])

def process_flow_data(file_path, year=2019):
    df = pd.read_csv(file_path)

    df['from'] = df['from'].map(lambda x: x if(eval(x)['name']) else None)
    df['to'] = df['to'].map(lambda x: x if(eval(x)['name']) else None)

    #df = df[df['year'] == year]

    # Evaluate dictionary parts
    df['from'] = df['from'].map(eval, na_action='ignore')
    df['to'] = df['to'].map(eval, na_action='ignore')

    # Prepare data for tooltip
    df["from_name"] = df["from"].apply(lambda f: f["name"])
    df["to_name"] = df["to"].apply(lambda t: t["name"])
    # Calculate additional data
    df["hospitalizations_rate"] = df["hospitalizations"].apply(lambda t: t*0.005)

    # Normalize and weight flow layer height to avoid z-fighting
    hosp_min, hosp_max = np.min(df["hospitalizations"]), np.max(df["hospitalizations"])
    df["height_rate"] = df["hospitalizations"].apply(lambda t: 0.3*(1-((t-hosp_min)/(hosp_max-hosp_min))))

    return df

def process_hosp_data(df):
    hosp_to = df[['to_name', 'to', 'hospitalizations']]
    hosp_to['to'] = hosp_to['to'].map(str)
    # Get the number of hospitalizations each municipality had
    hosp_to = hosp_to.groupby(by = ['to_name', 'to'], as_index = False).sum()
    # Calculate additional data
    hosp_to["hospitalizations_radius"] = hosp_to["hospitalizations"].apply(lambda hospitalizations: 10*math.sqrt(hospitalizations))
    hosp_to['to'] = hosp_to['to'].map(eval)

    return hosp_to

def get_state_latlong(uf):
    center = gpd.read_file("./data/%s/shapes/state/shape.dbf"%uf).centroid

    zoom = 6
    if(uf == 'AM'): zoom = 4.5
    if(uf == 'RJ'): zoom = 7
    if(uf == 'SP' or uf == 'RS'): zoom = 5.8
    if(uf == 'MG' or uf == 'MA' or uf == 'BA'): zoom = 5

    return [center.y[0], center.x[0], zoom]
    #state_center = {
    #    "RJ": [-22.19756813181387, -42.66442880031974, 7],
    #    "SP": [-22.26677449263794, -48.72790742500156, 5.8],
    #    "CE": [-5.09337418870375, -39.61586889725158, 6]
    #}
    #return state_center[uf]

def get_latlong_mean(df):
    lat, lon = [], []
    df.apply(lambda t: lat.append(t['latlong'][0]))
    df.apply(lambda t: lon.append(t['latlong'][1]))

    return round(statistics.mean(lon), 1), round(statistics.mean(lat), 1)

def get_shape_layer(uf, year, level_type, shape_type='gray', shape='default'):
    if(level_type=='mun'): gdf = gpd.read_file("./data/%s/shapes/state/shape.dbf"%uf)
    elif(level_type=='reg'): gdf = gpd.read_file("./data/%s/shapes/regions/shape.dbf"%uf)
    elif(level_type=='macro'): gdf = gpd.read_file("./data/%s/shapes/macroregions/shape.dbf"%uf)
    print(level_type, gdf)

    available_year = year
    if(year <= 2011): 
        available_year = 2011
        if(uf == 'SP'): available_year = 2012
    elif(year == 2018): available_year = 2017

    if(level_type=='reg'): gdf = gdf[gdf['year'] == available_year]
    elif(level_type=='macro'): gdf = gdf[gdf['year'] == 2019]

    if(shape_type=='gray'): line_width, color = 1, [150, 150, 150]
    elif(shape_type=='blue'): line_width, color = 5, [6, 150, 187, 100]

    if(shape=='mini'): line_width = 2

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=line_width,
        filled=True,
        get_line_color=color,
        get_fill_color=[150, 150, 150, 15],
        #pickable=True,
        #auto_highlight=True
    )
    #center = cascaded_union(gdf['geometry'].values).centroid.xy
    #print(uf, center)

    return layer

def get_communities_layer(uf, diag_type, level_type, year, shape='default'):
    if(diag_type=='all'): 
        if(level_type=='macro'): gdf = gpd.read_file("./data/%s/shapes/communities/macro/communities.dbf"%uf)
        else: gdf = gpd.read_file("./data/%s/shapes/communities/reg/communities.dbf"%uf)
    else:
        if(level_type=='macro'): gdf = gpd.read_file("./data/%s/shapes/communities/macro/%s_communities.dbf"%(uf, diag_type))
        else: gdf = gpd.read_file("./data/%s/shapes/communities/reg/%s_communities.dbf"%(uf, diag_type))
    
    # filter by year
    gdf = gdf[gdf['year'] == str(year)]

    if(shape=='mini'): line_width = 2
    else: line_width = 5

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=line_width,
        filled=True,
        get_line_color=[253, 198, 10, 50],
        get_fill_color=[150, 150, 150, 15],
    )

    return layer

def get_state_layer(uf):
    gdf = gpd.read_file("./data/%s/shapes/state/shape.shp"%uf)

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=5,
        filled=False,
        get_line_color=[97, 136, 85],
    )

    return layer

def get_map_layer(layer_type, df):
    if(layer_type=="flow"):
        layer = pdk.Layer(
            "GreatCircleLayer",
            df,
            pickable=True,
            getHeight="height_rate",
            getWidth="hospitalizations_rate",
            get_source_position="from.latlong",
            get_target_position="to.latlong",
            get_source_color=[0, 166, 255], #[0, 128, 200],
            get_target_color=[255, 131, 0], #[200, 128, 0],
            auto_highlight=True,
        )
        tooltip = {"text": "{from_name} para {to_name}\n{hospitalizations} internações"}
    else:
        layer = pdk.Layer(
            "ScatterplotLayer",
            df,
            pickable=True,
            opacity=0.8,
            stroked=True,
            filled=True,
            radius_scale=6,
            radius_min_pixels=1,
            radius_max_pixels=100,
            line_width_min_pixels=1,
            get_position="to.latlong",
            get_radius="hospitalizations_radius",
            get_fill_color=[200, 128, 0],
            get_line_color=[0, 0, 0],
        )
        tooltip = {"text": "{to_name}\n{hospitalizations} internações realizadas"}

    return layer, tooltip

def get_mini_comm_maps(uf, diag_value, level_value, year, lat, lon, zoom):
    # CREATE SHAPE LAYER
    shape_layer = get_shape_layer(uf, year, level_value, 'blue', shape='mini')

    # CREATE COMMUNITIES LAYER
    comm_layer = get_communities_layer(uf, diag_value, level_value, year, shape='mini')
    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=zoom/1.4,
    )

    r = pdk.Deck(shape_layer, initial_view_state=view_state)
    container1 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
        ),
        style={"height": "170px", "width": "100%", "position": "relative"},
    )

    r = pdk.Deck(comm_layer, initial_view_state=view_state)
    container2 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
        ),
        style={"height": "170px", "width": "100%", "position": "relative"},
    )

    return container1, container2

def color_scale(index, value):
    colors=[[194, 96, 4, 150],
            [210, 124, 61, 150],
            [222, 153, 105, 150],
            [232, 182, 149, 150],
            [238, 211, 194, 150],
            [198, 207, 215, 150],
            [155, 175, 190, 150],
            [113, 144, 165, 150],
            [70, 114, 141, 150],
            [4, 85, 117, 150]]

    if(value=='value'):
        if(index < 1): 
            if(index < 0.95): return colors[1]
            return colors[2]
        if(index > 1): 
            if(index > 1.05): return colors[-2]
            return colors[-3]
        return [255, 255, 255, 150]

    return colors[int(index*10//1)]

def to_1_lifo(x):
    return 1.0-x

def lifo_lofi_hmean(mlifo, lofi, w_mlifo=0.5, w_lofi=1.0):
    return (w_mlifo+w_lofi)/((w_mlifo/mlifo)+(w_lofi/lofi))

def get_coeff_layer(uf, year, diag_value, level_value, value="hmean", beta=0.5):
    if(level_value=='reg'): gdf = gpd.read_file("./data/%s/shapes/regions/shape.dbf"%uf)
    elif(level_value=='macro'): gdf = gpd.read_file("./data/%s/shapes/macroregions/shape.dbf"%uf)
    else: return ''

    available_year = year
    if(year <= 2011): 
        available_year = 2011
        if(uf == 'SP'): available_year = 2012
    elif(year > 2017): available_year = 2017

    if(level_value=='reg'): gdf = gdf[gdf['year'] == available_year]
    elif(level_value=='macro'): gdf = gdf[gdf['year'] == 2019]

    df = pd.read_csv("./data/%s/csv/coefficients/%s.csv"%(uf, level_value))
    df = df[df['year']==year]

    if(value=='hmean'): df["hmean"] = lifo_lofi_hmean(to_1_lifo(df['lifo']), df['lofi'], w_mlifo=beta)
    elif(value=='lifo'): 
        df['1-lifo'] = to_1_lifo(df['lifo'])
        value = '1-lifo'
    elif(value=='value'):
        df['value'] = df.apply(lambda row: (row['to_hospitalizations']+row['self_hospitalizations'])/(row['from_hospitalizations']+row['self_hospitalizations']), axis = 1)
        value = 'value'

    if(beta==0): df[value] = df['lofi']

    df[value] = df[value].round(3)

    gdf = gdf.merge(df[['name', value]], how='left', left_on=['name'], right_on=['name'])
    gdf['color'] = gdf[value].map(lambda i: color_scale(i, value))

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=2,
        filled=True,
        get_fill_color="color",
        pickable=True,
        auto_highlight=True,
    )

    return layer

def get_mini_coeff_maps(uf, diag_value, level_value, year, lat, lon, zoom):
    lofi_layer = get_coeff_layer(uf, year, diag_value, level_value, "lofi")
    lifo_layer = get_coeff_layer(uf, year, diag_value, level_value, "lifo")

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=zoom/1.4,
    )

    r = pdk.Deck(lofi_layer, initial_view_state=view_state)
    container1 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
            tooltip = {"html": "<b>{name}</b> <br /><b>Taxa de permanência:</b> {lofi}"}
        ),
        style={"height": "160px", "width": "100%", "position": "relative"},
    )

    r = pdk.Deck(lifo_layer, initial_view_state=view_state)
    container2 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
            tooltip = {"html": "<b>{name}</b> <br /><b>Taxa de atração:</b> {1-lifo}"}
        ),
        style={"height": "160px", "width": "100%", "position": "relative"},
    )

    return container1, container2
