# Dashboard Regionalização SUS

## Usage
```
git https://gitlab.com/gabriely.pereira/dashboard-regionalizacao-sus.git 
cd dashboard-regionalizacao-sus

virtualenv venv
source venv/bin/activate

pip install -r requirements.txt

# run
gunicorn --threads <number of desired threads> app:server

# or simply run the code bellow for a single thread
python3 app.py

```
