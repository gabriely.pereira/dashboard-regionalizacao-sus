import os
import time
import math
import statistics

import dash
import dash_deck
from dash import Input, Output, State, html
from dash import dcc
import dash_bootstrap_components as dbc
import pydeck as pdk
import plotly.express as px
import numpy as np
import pandas as pd
#from flask_caching import Cache

from components.patient_flow_tab import tab_flow
from components.detected_communities_tab import tab_communities
from components.coefficients_tab import tab_coefficients
from components.methodology_tab import tab_methodology
from components.about_tab import tab_about

from helpers import (
    process_flow_data, process_hosp_data, get_diagnosis, get_state_latlong, get_items_level,
    get_shape_layer, get_communities_layer, get_map_layer, get_mini_comm_maps, get_coeff_layer, 
    get_mini_coeff_maps, get_state_layer, 
)

mapbox_api_token = "pk.eyJ1IjoiZ2FicmllbHktcGVyZWlyYSIsImEiOiJjazV6ajFlcHEwZTFvM2RudTRwOGRkaDJ2In0.Pg8K-F29LH7GK3QiaUAwjA"#os.getenv("MAPBOX_ACCESS_TOKEN")

# CREATE APP
app = dash.Dash(__name__, title='Dashboard Regionalização no SUS', external_stylesheets=[dbc.themes.BOOTSTRAP])
server = app.server

#CACHE_CONFIG = {
#    # try 'FileSystemCache' if you don't want to setup redis
#    'CACHE_TYPE': 'FileSystemCache',
#    'CACHE_DIR': 'cache-directory'
#    #'CACHE_REDIS_URL': os.environ.get('REDIS_URL', 'redis://localhost:6379')
#}
#cache = Cache()
#cache.init_app(app.server, config=CACHE_CONFIG)

GLOBAL_DF = pd.DataFrame().to_json()

# CREATE COMPONENTS
dropdown_uf = dcc.Dropdown(
   options=[
       {'label': 'Amazonas', 'value': 'AM'},
       {'label': 'Bahia', 'value': 'BA'},
       {'label': 'Ceará', 'value': 'CE'},
       {'label': 'Espirito Santo', 'value': 'ES'},
       {'label': 'Maranhão', 'value': 'MA'},
       {'label': 'Pernambuco', 'value': 'PE'},
       {'label': 'Rio de Janeiro', 'value': 'RJ'},
       {'label': 'Rio Grande do Sul', 'value': 'RS'},
       {'label': 'São Paulo', 'value': 'SP'},
    ],
   value='RJ',
   searchable=True,
   clearable=False,
   id="uf_drop",
   style={'font-size': 14}
)

dropdown_diag = dcc.Dropdown(
   options=get_diagnosis(),
   value='all',
   searchable=True,
   clearable=False,
   id="diag_drop",
   style={'font-size': 14}
)

dropdown_level = dcc.Dropdown(
   options=[
       {'label': 'Municípios', 'value': 'mun'},
       {'label': 'Regiões de Saúde', 'value': 'reg'},
       {'label': 'Macrorregiões de Saúde', 'value': 'macro'},
    ],
   value='mun',
   clearable=False,
   id="level_drop",
   style={'font-size': 14}
)

tabs = dbc.Tabs(
    [
        dbc.Tab(tab_flow(app), label="Fluxo de pacientes", tab_id="tab1"),
        dbc.Tab(tab_communities(app), label="Divisões sugeridas", tab_id="tab2"),
        dbc.Tab(tab_coefficients(app), label="Permanência vs. atração", tab_id="tab3"),
        dbc.Tab(tab_methodology(app), label="Metodologia", tab_id="tab4"),
        dbc.Tab(tab_about(app), label="Sobre", tab_id="tab5"),
    ], active_tab="tab1",
    className="justify-content-center nav-fill", id="tabs_id"
)

app.layout = html.Div(
    className="container-fluid text-white bg-dark full-height",
    children=[
        html.Br(),
        dbc.Row(
            [
                dbc.Col([html.Img(src=app.get_asset_url('interscity_logo.png'), height="50"), 
                        html.Img(src=app.get_asset_url('ieps_logo.png'), height="40")], width=3),
                dbc.Col(html.H3(children="Dashboard Regionalização no SUS"), width=3),
                dbc.Col(["Escolha o estado:", dropdown_uf], width=2),
                dbc.Col(["Diagnóstico:", html.Div(dropdown_diag, id="diag_drop_father")], width=2),
                dbc.Col(["Nível da análise:", dropdown_level], width=2),
            ]
        ),
        tabs,

        # Hidden div inside the app that stores the intermediate value
        html.Div(id='intermediate-value', style={'display': 'none'}, children = GLOBAL_DF),
        # signal value to trigger callbacks
        dcc.Store(id='signal')
    ],
)


#@cache.memoize()
def global_store(value):
    uf, diag, level = value

    # simulate expensive query
    print(f'~~~~~~~~~~~~~ Computing value with {uf}, {diag} and {level}')

    start_time = time.time()
    file_path = "./data/%s/csv/%s_sih_flow.csv"%(uf, level)
    if(diag!='all'): file_path = "./data/%s/csv/diagnosis/%s/%s_%s_sih_flow.csv"%(uf, level, level, diag)
    
    result = process_flow_data(file_path)

    print("--- %s seconds ---" % (time.time() - start_time))
    
    return result

# CREATE CALLBACK FUNCTIONS
#   TAB1 CALLBACKS
@app.callback([Output('signal', 'data'), 
               Output('intermediate-value', 'children')],
              [Input("uf_drop", "value"),
               Input("diag_drop", "value"),
               Input("level_drop", "value"),
               State('signal', 'data')])
def compute_value(uf, diag, level, last_signal):
    signal = [uf, diag, level]
    # compute value and send a signal when done
    if(signal != last_signal): 
        print("---------------- signal -----------------")
        GLOBAL_DF = global_store(signal).to_json()
        print("-------------- end signal ---------------")
    return signal, GLOBAL_DF


@app.callback(Output("available_data_flow", "children"),
              [Input("year_slider_flow", "value"),
               Input("level_drop", "value")]
              )
def show_available_data_msg(year, level_value):
    if(level_value == 'mun'): return html.Div()
    if(level_value == 'macro'): 
        return "Por falta de disponibilidade da base completa, aqui utilizamos o limite macroregional de 2019."

    available_year = year
    if(year < 2011): available_year = 2011
    elif(year == 2018): available_year = 2017

    if(available_year == year): return html.Div()
    return "Por falta de disponibilidade da base completa, aqui utilizamos o limite regional de %s."%available_year

@app.callback(Output("legend", "style"),
              Input("map_radios", "value"))
def switch_legend(ratio_value):
    if(ratio_value=='flow'):
        return {'display': 'block',
                'position': 'absolute',
                'bottom': '60px',
                'right': '35px'}
    else: return {'display': 'None'}

@app.callback([Output("diagnosis_span_flow", "children"),
               Output("year_span_flow", "children")], 
              [Input("diag_drop", "value"),
               Input("year_slider_flow", "value")])
def get_diagnosis_name_flow(diag_value, year):
    return " | " + get_diagnosis(diag_value), year

@app.callback([Output("choose_bar_level", "children"),
               Output("dropdown_bar_level", "children")], 
              [Input("uf_drop", "value"),
               Input("year_slider_flow", "value"),
               Input("level_drop", "value")])
def display_bar_charts(uf, year, level_value):
    options = get_items_level(uf, year, level_value)
    dropdown = dcc.Dropdown(
        options=options,
        value=options[0],
        searchable=True,
        clearable=False,
        id="level_items_drop",
    )
    
    if(level_value=='reg'): return "Escolha a região de saúde:", dropdown
    if(level_value=='macro'): return "Escolha a macrorregião de saúde:", dropdown
    return "Escolha a cidade:", dropdown

@app.callback([Output("top5_inflow_output", "children"),
               Output("top5_outflow_output", "children")], 
              [Input("signal", "data"),
               Input("year_slider_flow", "value"),
               Input("level_items_drop", "value")], State('intermediate-value', 'children'))
def display_top5(signal, year, level_items, GLOBAL_DF):
    # IMPORT AND PROCESS DATA

    top5_df = pd.read_json(GLOBAL_DF)

    top5_df = top5_df[top5_df['year'] == int(year)]

    df_inflow = top5_df[top5_df['to_name']==level_items].sort_values(by='hospitalizations', ascending=False).head(5)
    df_outflow = top5_df[top5_df['from_name']==level_items].sort_values(by='hospitalizations', ascending=False).head(5)

    others_sum = top5_df[top5_df['to_name']==level_items]['hospitalizations'].sum() - df_inflow['hospitalizations'].sum()
    others = pd.DataFrame([['Outros', 'Outros', others_sum]],
                   columns=['from_name', 'to_name', 'hospitalizations'])
    if(others_sum > 0): df_inflow = pd.concat([df_inflow, others])

    others_sum = top5_df[top5_df['from_name']==level_items]['hospitalizations'].sum() - df_outflow['hospitalizations'].sum()
    others = pd.DataFrame([['Outros', 'Outros', others_sum]],
                   columns=['from_name', 'to_name', 'hospitalizations'])
    if(others_sum > 0): df_outflow = pd.concat([df_outflow, others])

    bar_chart_inflow = dcc.Graph(
        figure=px.bar(
            df_inflow.iloc[::-1], title="Internações recebidas em %s"%level_items,
            x="hospitalizations", y="from_name", orientation='h', height=230, text_auto=True,
            color_discrete_sequence=px.colors.qualitative.Pastel[1:], template="plotly_dark", 
            labels={'hospitalizations':'Internações', 'from_name':'Origem'},
        ).update_layout(margin={"r":0,"t":46,"l":0,"b":6}).update_xaxes(visible=False).update_xaxes(title_text='Internações').update_traces(texttemplate='%{x:.d}')
    )

    bar_chart_outflow = dcc.Graph(
        figure=px.bar(
            df_outflow.iloc[::-1], title="Internações que partiram de %s"%level_items,
            x="hospitalizations", y="to_name", orientation='h', height=230, text_auto=True,
            color_discrete_sequence=px.colors.qualitative.Pastel, template="plotly_dark", 
            labels={'hospitalizations':'Internações', 'to_name':'Destino'},
        ).update_layout(margin={"r":0,"t":46,"l":0,"b":6}).update_xaxes(visible=False).update_traces(texttemplate='%{x:.d}')
    )

    return bar_chart_inflow, bar_chart_outflow

@app.callback(Output("map_output_flow", "children"), 
             [Input("signal", "data"),
             Input("year_slider_flow", "value"),
             Input("map_radios", "value")], State('intermediate-value', 'children'))
def display_flow_map(signal, year, ratio_value, GLOBAL_DF):
    # IMPORT AND PROCESS DATA
    uf, _, level_value = signal

    flow_df = pd.read_json(GLOBAL_DF)

    flow_df = flow_df[flow_df['year'] == int(year)]

    # CREATE SHAPE LAYER
    shape_layer = get_shape_layer(uf, year, level_value)

    # CREATE MAP LAYER
    if(ratio_value == "flow"): layer, tooltip = get_map_layer("flow", flow_df)
    else: layer, tooltip = get_map_layer("scatter", process_hosp_data(flow_df))

    # Define the map center based on shape
    lat, lon, zoom = get_state_latlong(uf)

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=zoom,
    )

    layers = [shape_layer, layer]
    r = pdk.Deck(layers, initial_view_state=view_state, map_provider='mapbox', api_keys={'mapbox': mapbox_api_token})#, mapbox_key=mapbox_api_token, map_style='mapbox://styles/gabriely-pereira/cl5beumw4002c15sbwdv80ex0')

    deck_container = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl", 
            tooltip=tooltip, 
            mapboxKey=r.mapbox_key
        ),
        style={"height": "550px", "width": "100%", "position": "relative"},
    )
    return deck_container


#   TAB2 CALLBACKS
@app.callback(Output("available_data_comm", "children"),
              [Input("year_slider_comm", "value"),
               Input("level_drop", "value")]
              )
def show_available_data_msg(year, level_value):
    if(level_value == 'mun'): return html.Div()
    if(level_value == 'macro'): 
        return "Por falta de disponibilidade da base completa, aqui utilizamos o limite macroregional de 2019."

    available_year = year
    if(year < 2011): available_year = 2011
    elif(year == 2018): available_year = 2017

    if(available_year == year): return html.Div()
    return "Por falta de disponibilidade da base completa, aqui utilizamos o limite regional de %s."%available_year

@app.callback(Output("legend_comm", "style"),
              Input("tabs_id", "active_tab"))
def display_comm_legend(tab_value):
    if(tab_value=='tab2'):
        return {'display': 'block',
                'position': 'absolute',
                'bottom': '60px',
                'right': '35px'}
    else: return {'display': 'None'}

@app.callback([Output("level_drop", "options"),
               Output("level_drop", "value")],
              Input("tabs_id", "active_tab"),
              State("level_drop", "value"))
def change_level_options(tab_value, level_state):
    value = level_state

    if(tab_value=='tab2' or tab_value=='tab3'):
        if(level_state=='mun'): value = 'reg'

        return [{'label': 'Regiões de Saúde', 'value': 'reg'},
                {'label': 'Macrorregiões de Saúde', 'value': 'macro'}], value
    return [
            {'label': 'Municípios', 'value': 'mun'},
            {'label': 'Regiões de Saúde', 'value': 'reg'},
            {'label': 'Macrorregiões de Saúde', 'value': 'macro'}], value

@app.callback([Output("diagnosis_span_comm", "children"),
               Output("year_span_comm", "children")], 
              [Input("diag_drop", "value"),
               Input("year_slider_comm", "value")])
def get_diagnosis_name_comm(diag_value, year):
    return " | " + get_diagnosis(diag_value), year

@app.callback([Output("map_output_comm", "children"),
              Output("mini_shape_reg", "children"),
              Output("mini_shape_comm", "children")],
             [Input("uf_drop", "value"),
              Input("year_slider_comm", "value"),
              Input("diag_drop", "value"), 
              Input("level_drop", "value"),
              Input("tabs_id", "active_tab")])
def display_comm_map(uf, year, diag_value, level_value, tab_value):
    # CREATE SHAPE LAYER
    shape_layer = get_shape_layer(uf, year, level_value, 'blue')

    # CREATE COMMUNITIES LAYER
    comm_layer = get_communities_layer(uf, diag_value, level_value, year)
    
    # Define the map center based on shape
    lat, lon, zoom = get_state_latlong(uf)

    mini_deck1, mini_deck2 = get_mini_comm_maps(uf, diag_value, level_value, year, lat, lon, zoom)

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=zoom,
    )

    layers = [shape_layer, comm_layer, get_state_layer(uf)]
    r = pdk.Deck(layers, initial_view_state=view_state, map_provider='mapbox', api_keys={'mapbox': mapbox_api_token})#, mapbox_key=mapbox_api_token, map_style='mapbox://styles/gabriely-pereira/cl5beumw4002c15sbwdv80ex0')

    deck_container = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl", 
            #tooltip=tooltip, 
            mapboxKey=r.mapbox_key
        ),
        style={"height": "550px", "width": "100%", "position": "relative"},
    )
    return deck_container, mini_deck1, mini_deck2

@app.callback([Output("line_chart_output_comm", "children"),
               Output("similarity_level", "children")],
              [Input("uf_drop", "value"),
               Input("year_slider_comm", "value"),
               Input("diag_drop", "value"),
               Input("level_drop", "value")])
def display_similarity_comm(uf, year, diag_value, level_value):
    if(level_value=='mun'): return html.Div(), html.Div()
    df = pd.read_csv("./data/%s/csv/similarity/%s.csv"%(uf, level_value))

    line_chart_similarity = dcc.Graph(
        figure=px.line(df, x='year', y=diag_value, #color='country', 
                        labels={'year':'Ano', diag_value:'Similaridade (%)'},
                        markers=True, height=227, template="plotly_dark", 
                        hover_data={diag_value:':.1f'},
                        color_discrete_sequence=px.colors.sequential.Greens[4:])
        .update_layout(margin={"r":5,"t":5,"l":5,"b":5}).update_yaxes(range=[0, 100])#.update_xaxes(visible=False)
        .add_shape(dict(type="line", x0=year, y0=0, x1=year, y1=100,
                        line=dict(color="white", width=3, dash="dot"))
        )
    )
    return line_chart_similarity, "%.1f%%"%df[df['year']==year][diag_value]


#   TAB3 CALLBACKS
@app.callback(Output("diag_drop_father", "children"),
              Input("tabs_id", "active_tab"))
def disable_dropdown(tab_value):
    if(tab_value == "tab2" or tab_value == "tab3"):
        return dcc.Dropdown(
                options={"all": "Todos os diagnósticos (fixo)"},
                disabled=True,
                value='all',
                searchable=True,
                clearable=False,
                id="diag_drop",
                style={'font-size': 14}
            )
    return dropdown_diag
    
@app.callback(Output("available_data_coeff", "children"),
              [Input("year_slider_coeff", "value"),
               Input("level_drop", "value")]
              )
def show_available_data_msg(year, level_value):
    if(level_value == 'mun'): return html.Div()
    if(level_value == 'macro'): 
        return "Por falta de disponibilidade da base completa, aqui utilizamos o limite macroregional de 2019."

    available_year = year
    if(year < 2011): available_year = 2011
    elif(year == 2018): available_year = 2017

    if(available_year == year): return html.Div()
    return "Por falta de disponibilidade da base completa, aqui utilizamos o limite regional de %s."%available_year

@app.callback([Output("diagnosis_span_coeff", "children"),
               Output("year_span_coeff", "children")], 
              [Input("diag_drop", "value"),
               Input("year_slider_coeff", "value")])
def get_diagnosis_name_coeff(diag_value, year):
    #return " | " + get_diagnosis(diag_value), year
    return "", year

@app.callback([Output("map_output_coeff", "children"),
               Output("mini_lofi", "children"),
               Output("mini_1-lifo", "children")],
             [Input("uf_drop", "value"),
              Input("year_slider_coeff", "value"),
              Input("diag_drop", "value"), 
              Input("level_drop", "value"),
              Input("atracao_slider_coeff", "value")])
def display_coefficients_map(uf, year, diag_value, level_value, beta):
    # CREATE SHAPE LAYER
    coeff_layer = get_coeff_layer(uf, year, diag_value, level_value, value="hmean", beta=beta)

    # Define the map center based on shape
    lat, lon, zoom = get_state_latlong(uf)

    mini1, mini2 = get_mini_coeff_maps(uf, diag_value, level_value, year, lat, lon, zoom)

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=zoom,
    )

    r = pdk.Deck(coeff_layer, initial_view_state=view_state, map_provider='mapbox', api_keys={'mapbox': mapbox_api_token})#, mapbox_key=mapbox_api_token, map_style='mapbox://styles/gabriely-pereira/cl5beumw4002c15sbwdv80ex0')
    tooltip = {"html": "<b>{name}</b> <br /><b>Valor:</b> {hmean}"}

    deck_container = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl", 
            tooltip=tooltip, 
            mapboxKey=r.mapbox_key
        ),
        style={"height": "550px", "width": "100%", "position": "relative"},
    )
    return deck_container, mini1, mini2


if __name__ == "__main__":
    app.run_server(debug=False)
